package features.produtos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class ProdutosTest {

    private WebDriver navegador;

    @BeforeEach
    public void beforeEach() {

        System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver-win64\\chromedriver-win64\\chromedriver.exe");
        this.navegador = new ChromeDriver();

        this.navegador.manage().window().maximize();

        this.navegador.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        this.navegador.get("https://www.saucedemo.com/");
    }

    @Test
    @DisplayName("Adicionar Produto no carrinho")
    public void addProdutoCarrinho() {
        navegador.findElement(By.id("user-name")).sendKeys("standard_user");
        navegador.findElement(By.id("password")).sendKeys("secret_sauce");
        navegador.findElement(By.id("login-button")).click();
        navegador.findElement(By.id("add-to-cart-sauce-labs-backpack")).click();
         String qtdProdutosCarrinho = navegador.findElement(By.cssSelector("span[class='shopping_cart_badge']")).getText();

        Assertions.assertEquals("1",qtdProdutosCarrinho);

    }


    @Test
    @DisplayName("Remover Produto do Carrinho")
    public void removerProdutoCarrinho(){
        navegador.findElement(By.id("user-name")).sendKeys("standard_user");
        navegador.findElement(By.id("password")).sendKeys("secret_sauce");
        navegador.findElement(By.id("login-button")).click();
        navegador.findElement(By.id("add-to-cart-sauce-labs-backpack")).click();
        navegador.findElement(By.id("remove-sauce-labs-backpack")).click();
        //Validar que o carrinho esta vazio

    }

    @Test
    @DisplayName("Detalhes do Produto")
    public void detalharProduto() {
        navegador.findElement(By.id("user-name")).sendKeys("standard_user");
        navegador.findElement(By.id("password")).sendKeys("secret_sauce");
        navegador.findElement(By.id("login-button")).click();
        String nomeProduto = navegador.findElement(By.cssSelector(".inventory_item_name")).getText();
        navegador.findElement(By.cssSelector(".inventory_item_name")).click();
        System.out.println(nomeProduto);
        Assertions.assertEquals("Sauce Labs Backpack",nomeProduto);



    }




}
