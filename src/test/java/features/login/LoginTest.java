package features.login;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;


@DisplayName("Teste Web do Modulo Login")
public class LoginTest {

    private WebDriver navegador;

    @BeforeEach
    public void beforeEach() {

        System.setProperty("webdriver.chrome.driver","C:\\drivers\\chromedriver-win64\\chromedriver-win64\\chromedriver.exe");
        this.navegador = new ChromeDriver();

        this.navegador.manage().window().maximize();

        this.navegador.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        this.navegador.get("https://www.saucedemo.com/");

    }
    @Test
    @DisplayName("Realizar Login com dados Validos")
    public void testLoginDadosValidos () {

        navegador.findElement(By.id("user-name")).sendKeys("standard_user");
        navegador.findElement(By.id("password")).sendKeys("secret_sauce");
        navegador.findElement(By.id("login-button")).click();
        String telaTitle = navegador.findElement(By.cssSelector("span[class='title']")).getText();
        Assertions.assertEquals("Products",telaTitle);

    }

    @Test
    @DisplayName("Realizar Logout ")
    public void testLogout() {
        navegador.findElement(By.id("user-name")).sendKeys("standard_user");
        navegador.findElement(By.id("password")).sendKeys("secret_sauce");
        navegador.findElement(By.id("login-button")).click();
        String telaTitle = navegador.findElement(By.cssSelector("span[class='title']")).getText();
        Assertions.assertEquals("Products",telaTitle);
        navegador.findElement(By.id("react-burger-menu-btn")).click();
        navegador.findElement(By.id("logout_sidebar_link")).click();
        //String botaoLogin = navegador.findElement(By.id("login-button")).getCssValue();






    }
}
